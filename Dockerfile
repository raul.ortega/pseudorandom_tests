FROM alpine:3.15.4
RUN set -x && \
  apk add --no-cache --update \
    nano \
    gcc \
    g++ \
    libstdc++ \
    gcompat
WORKDIR /

COPY test_cstdlib.sh .
COPY test_mt19937.sh .
COPY run.sh .

RUN chmod +x *.sh

CMD ["/bin/sh", "run.sh"]
