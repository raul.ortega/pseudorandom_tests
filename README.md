# pseudorandom_tests

## overview

This repository illustrates the problem of generating pseudorandom numbers using the srand and rand functions of the cstdlib library in c++.

The bash run_all.sh script generates two images, one based on ubuntu 20.04 and one based on alpine 3.15.4.

In both, the necessary packages are installed to compile and execute the c++ code that generates a random number based on the cstdlib library and another random number based on the random library.

It can be verified that using the same seed, the random number generated with the rand() function of the cstdlib library is different from the number generated with the mt() function of the random library.

## get started

```sh
# clone repository
git clone https://gitlab.com/raul.ortega/pseudorandom_tests.git

# jump into folder
cd pseudorandom_tests

# change permissions
chmod +x run_all.sh

# execute
./run_all.sh
```

## results
```
generate random numbers with ubuntu
srand(1);rand(): 1804289383
mt(1): 1791095845

generate random numbers with alpine
srand(1);rand(): 0
mt(1): 1791095845
```
