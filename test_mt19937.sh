#!/bin/bash

# C++ code
cpp_code=$(cat << 'EOF'
#include <iostream>
#include <random>
using namespace std;
  
int main()
{
  // Initializing the sequence 
  // with a seed value
  // similar to srand()
  mt19937 mt(1); 
  
  // Printing a random number
  // similar to rand()
  cout << mt() << '\n'; 
  return 0;
}
EOF
)

# binary file 
executable="test_mt19937"

# Compile
g++ -o $executable -x c++ - << EOF
$cpp_code
EOF

if [ $? -eq 0 ]; then
   echo "mt(1): "$(./$executable)
else
    echo "Error"
fi
