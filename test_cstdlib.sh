#!/bin/bash

# C++ code
cpp_code=$(cat << 'EOF'
#include <iostream>
#include <cstdlib>
#include <ctime>

int main() {
    srand(1); // set seed value

    int randomNumber = rand();

    std::cout << randomNumber << std::endl;

    return 0;
}
EOF
)

# binary file
executable="test_cstlib"

# Compile
g++ -o $executable -x c++ - << EOF
$cpp_code
EOF

if [ $? -eq 0 ]; then
   echo "srand(1);rand(): "$(./$executable)
else
    echo "Error"
fi
