#!/bin/bash

echo "build ubuntu image"

# build image based on ubuntu
docker build -f DockerfileUbuntu -t ubuntu_test_random .


echo "build alpine image"

# build image based on alpine
docker build -f Dockerfile -t alpine_test_random .

echo "generate random numbers with ubuntu"
docker run ubuntu_test_random

echo "generate random numbers with alpine"
docker run alpine_test_random
